<?php
$servername = "localhost";
$username = "root";
$password = "iamroot";
$dbname = "hello";
$tbl = "hi";

// connect
$conn = new mysqli($servername, $username, $password, $dbname);
// check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// retrieve and display data
$sql = "select greeting from hi";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        echo $row["greeting"] . ".<br/>";
    }
} else {
    echo "No results";
}
?>